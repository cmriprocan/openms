# Change Log

### v1.2.4

- Updated `PyProphet` to `2.0.4`, fixing bug in backpropogate command

### v1.2.3

- Bumped OpenMS to version  `CMRI-ProCan-v1.1.2` from the `CMRI-ProCan/OpenMS` repository that includes hotfix from upstream (`OpenMS/OpenMS@master`) for bug OpenMS#3860 that solves issues of NaNs crashing the SQLite workflow
- Bumped PyProphet to version `2.0.2` or `185873a3b0a2c02a94c20a6f55b58178d0284200` that includes two bug fixes relating to: target-decoy distributions that don't overlap, and backpropagating scores that haven't had protein FDR calculated.
