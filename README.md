# openms

An install of the OpenMS tools that aims to be as lightweight as possible. Includes Miniconda 3 and tools for interacting with AWS.

An example for running in a VM on the HPC:


    # if you haven't already mounted prostor1-DAS1
    sudo mkdir /mnt/prostor1
    sudo mount -t cifs -o username=`whoami`,uid=$UID "//cmri-prostor1.cmri.com.au/Procan Storage - DAS 1" /mnt/prostor1
    
    # run SwathScores docker pipeline
    mkdir /tmp/pipeline-test
    cd /tmp/pipeline-test
    export OPENMS_VERSION=1.0.4
    export PROCAN90_DIR=/mnt/prostor1/datasets/ProCan90
    export SRL=170825_pc4_HEK_2Dcon__human_SwissProt_canonical_28022018.top6_fragments.reverse_decoys.tsv
    export iRT_SRL=170825_pc4_HEK_2Dcon__human_SwissProt_canonical_28022018.top6_fragments.openswath_iRT.TraML
    docker run \
           --rm \
           -v `pwd`:/scratch \
           -v $PROCAN90_DIR:/data \
           -ti cmriprocan/openms:$OPENMS_VERSION \
           OpenSwathWorkflow -in /data/mzML/ProCan90-M01-01.mzML \
                             -tr /data/library/$SRL \
                             -tr_irt /data/library/$iRT_SRL \
                             -out_tsv /scratch/ProCan90-M01-01.OSW.tsv \
                             -sort_swath_maps \
                             -threads `nproc` \
                             -min_upper_edge_dist 1 \
                             -readOptions cache \
                             -tempDirectory /scratch/.cache-ProCan90-M01-01 \
                             -force
