# Copyright (c) Jupyter Development Team.
# Copyright (c) CMRI-ProCan Team.
# Distributed under the terms of the Modified BSD License.

FROM ubuntu:18.04

# Metadata
LABEL website="http://www.cmri.org.au/ProCan"
LABEL author="ProCan Software Engineering <procansoftengrobot@cmri.org.au>"

USER root

# Install all OS dependencies
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update \
 && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
        software-properties-common \
        apt-utils \
        locales \
        gcc \
        g++ \
        wget \
        git \
        cmake \
        make \
        autotools-dev \
        automake \
        autoconf \
        qt4-dev-tools \
        patch \
        libtool \
        libsvm-dev \
        libglpk-dev \
        libqt4-dbg \
        libqt4-dev \
        libqt4-opengl-dev \
        libqtwebkit-dev \
        coinor-libcoinutils-dev \
        coinor-libcbc-dev \
        coinor-libcgl-dev \
        coinor-libclp-dev \
        coinor-libosi-dev \
        libtbb-dev \
        libcppunit-dev \
        liblog4cplus-dev \
        libglfw3-dev \
        libopenexr-dev \
        libilmbase-dev \
	parallel \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
 && locale-gen

# Install Tini
RUN wget --quiet https://github.com/krallin/tini/releases/download/v0.10.0/tini \
 && echo "1361527f39190a7338a0b434bd8c88ff7233ce7b9a4876f3315c22fce7eca1b0 *tini" | sha256sum -c - \
 && mv tini /usr/local/bin/tini \
 && chmod +x /usr/local/bin/tini

# Configure environment
ENV CONDA_DIR=/opt/conda \
    SHELL=/bin/bash \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8
ENV PATH=${CONDA_DIR}/bin:$PATH

# Install conda as jovyan and check the md5 sum provided on the download site
# take care to ensure that the channels for conda are specified in the correct order
# only change this if you are sure
ENV MINICONDA_VERSION 4.5.4
ENV CONDA_VER 4.6.14
RUN cd /tmp \
 && wget --quiet https://repo.continuum.io/miniconda/Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh \
 && echo "a946ea1d0c4a642ddf0c3a26a18bb16d *Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh" | md5sum -c - \
 && /bin/bash Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh -f -b -p $CONDA_DIR \
 && rm Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh \
 && $CONDA_DIR/bin/conda config --system --prepend channels conda-forge \
 && $CONDA_DIR/bin/conda config --system --prepend channels defaults \
 && $CONDA_DIR/bin/conda config --system --set auto_update_conda false \
 && $CONDA_DIR/bin/conda config --system --set show_channel_urls true \
 && $CONDA_DIR/bin/conda update --all --quiet --yes \
 && $CONDA_DIR/bin/conda install --yes python=3.6.* conda=$CONDA_VER \
 && $CONDA_DIR/bin/pip install --upgrade pip \
 && $CONDA_DIR/bin/conda install --yes \
        bzip2>=1.0 \
        unzip>=6.0 \
        xerces-c>=3.2 \
        awscli>=1.14.46 \
        boto3>=1.4.4 \
        pandas>=0.20.3 \
        h5py>=2.6.0 \
        pytables>=3.4.2 \
        Cython>=0.28 \
        boost=1.67.0 \
        libboost=1.67.0 \
        hdf5=1.10.2 \
        numpy=1.15.1 \
        qt>=5.* \
 && $CONDA_DIR/bin/conda clean -tipsy \
 && rm -rf /home/$NB_USER/.cache/yarn

# ---
# install OpenMS contrib libraries
ENV INSTALL_PARENT_DIR=/opt
ENV OPENMS_CONTRIB_DIR=${INSTALL_PARENT_DIR}/OpenMS-contrib
RUN mkdir -p ${INSTALL_PARENT_DIR} \
 && git clone https://github.com/OpenMS/contrib ${OPENMS_CONTRIB_DIR} \
 && mkdir -p ${OPENMS_CONTRIB_DIR}/build \
 && cd ${OPENMS_CONTRIB_DIR}/build \
 && cmake -DNUMBER_OF_JOBS=$(($(nproc) + 1)) -DBUILD_TYPE=SEQAN .. \
 && cmake -DNUMBER_OF_JOBS=$(($(nproc) + 1)) -DBUILD_TYPE=WILDMAGIC .. \
 && cmake -DNUMBER_OF_JOBS=$(($(nproc) + 1)) -DBUILD_TYPE=EIGEN .. \
 && rm -rf ${OPENMS_CONTRIB_DIR}/build/CMake* \
 && rm -rf ${OPENMS_CONTRIB_DIR}/build/src

# ---
# install OpenMS
RUN mkdir -p ${INSTALL_PARENT_DIR} \
 && cd ${INSTALL_PARENT_DIR} \
 && git clone https://github.com/CMRI-ProCan/OpenMS ${INSTALL_PARENT_DIR}/OpenMS \
 && cd ${INSTALL_PARENT_DIR}/OpenMS \
 && git checkout CMRI-ProCan-v1.1.2 \
 && mkdir -p ${INSTALL_PARENT_DIR}/OpenMS/build \
 && cd ${INSTALL_PARENT_DIR}/OpenMS/build \
 && cmake \
        -DCMAKE_PREFIX_PATH="${OPENMS_CONTRIB_DIR};${OPENMS_CONTRIB_DIR}/build;/usr/;/usr/local;${CONDA_DIR}" \
        -DCMAKE_BUILD_TYPE="RelWithDebInfo" \
        -DBOOST_USE_STATIC=OFF \
        -DMT_ENABLE_TBB=ON \
        -DHAS_XSERVER=Off \
        -DWITH_GUI=OFF \
        -DPYOPENMS=OFF \
        -DOPENMS_CONTRIB_LIBS:FILEPATH="${OPENMS_CONTRIB_DIR}" \
        -DQT_QMAKE_EXECUTABLE:FILEPATH="${CONDA_DIR}/bin/qmake" \
        -DBOOST_ROOT=${CONDA_DIR} \
        ${INSTALL_PARENT_DIR}/OpenMS \
 && make -j $(($(nproc) + 1)) OpenSwathWorkflow \
 && make -j $(($(nproc) + 1)) TargetedFileConverter \
 && make -j $(($(nproc) + 1)) OpenSwathDecoyGenerator \
 && rm -rf ${INSTALL_PARENT_DIR}/OpenMS/.git \
 && rm -rf ${INSTALL_PARENT_DIR}/OpenMS/src/tests \
 && rm -rf ${INSTALL_PARENT_DIR}/OpenMS/build/CMake* \
 && rm -rf ${INSTALL_PARENT_DIR}/OpenMS/build/pyOpenMS \
 && echo "******************************************************" \
 && ${INSTALL_PARENT_DIR}/OpenMS/build/bin/OpenSwathWorkflow --help

ENV PATH=${INSTALL_PARENT_DIR}/OpenMS/build/bin:$PATH

# ---
# install pyprophet (version 2.0.4 -- the latest version on master @ 2018/12/15)
RUN $CONDA_DIR/bin/pip install git+https://github.com/PyProphet/pyprophet.git@d35a53af86131e7c4eb57bbb09be8935a1f30c70 \
 && echo "******************************************************" \
 && $CONDA_DIR/bin/pyprophet --version

# ---
# install tric (this is the latest version on master @ 2018/11/15)
RUN mkdir -p ${INSTALL_PARENT_DIR} \
 && cd ${INSTALL_PARENT_DIR} \
 && git clone https://github.com/carljv/Will_it_Python.git ${INSTALL_PARENT_DIR}/Will_it_Python \
 && cd ${INSTALL_PARENT_DIR}/Will_it_Python \
 && git checkout 5feb80e4023d73bff12040c5aa5f14177794d459 \
 && cd ${INSTALL_PARENT_DIR}/Will_it_Python//MLFH/CH2/lowess\ work/ \
 && $CONDA_DIR/bin/python setup.py build && $CONDA_DIR/bin/python setup.py install \
 && git clone https://github.com/msproteomicstools/msproteomicstools.git ${INSTALL_PARENT_DIR}/msproteomicstools \
 && cd ${INSTALL_PARENT_DIR}/msproteomicstools \
 && git checkout eeed76521493247fec4b9979c068628103aef0bc \
 && $CONDA_DIR/bin/python setup.py build --with_cython && $CONDA_DIR/bin/python setup.py install \
 && echo "******************************************************" \
 && $CONDA_DIR/bin/feature_alignment.py --help

# Configure container startup
ENTRYPOINT ["tini", "--"]

# Overwrite this with 'CMD []' in a dependent Dockerfile
CMD ["/bin/bash"]

# add the fetch and run script so we can use AWS
ADD fetch_and_run.sh /usr/local/bin/fetch_and_run.sh
RUN chmod a+x /usr/local/bin/fetch_and_run.sh

ENV BASHRC_FILE=/root/.bashrc
RUN echo "" >> $BASHRC_FILE \
 && echo "##################################" >> $BASHRC_FILE \
 && echo "# Aliases" >> $BASHRC_FILE \
 && echo "##################################" >> $BASHRC_FILE \
 && echo "alias ll='ls -halF'" >> $BASHRC_FILE \
 && echo "alias la='ls -hA'" >> $BASHRC_FILE \
 && echo "alias l='ls -hlF'" >> $BASHRC_FILE \
 && echo "alias ..='cd ..'" >> $BASHRC_FILE \
 && echo "alias ...='cd ../..'" >> $BASHRC_FILE \
 && echo "" >> $BASHRC_FILE
